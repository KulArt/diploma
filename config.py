from parsers.meduza import MeduzaParser
from parsers.lenta import LentaParser
from parsers.vedomosti import VedomostiParser
from parsers.rbc import RbcParser
from parsers.tass import TassParser
from parsers.commersant import CommersantParser
from parsers.ria import RiaParser
from parsers.rt import RtParser
from parsers.yandex import YaParser
from parsers.bbc import BbcParser

from connectors.greedy_connector import GreedyConnector
from connectors.mle_connector import MLEConnector
from connectors.greedy_connector_with_used import GreedyConnectorWithUsed

from vectorizers.TfIdf import ComplexTfIdfVectorizer
from vectorizers.RuBert import RuBertVectorizer

from clusterizer.agglomerative import AgglomerativeClusterizer
from clusterizer.BertBased import BertBasedClusterizer
from clusterizer.LDA import LDAClusterizer

from distancers.centroid import CentroidDistancer
from distancers.singular import SingularDistancer

import datetime

RU_MONTH_VALUES = {
    'январь': 1,
    'января': 1,
    'февраль': 2,
    'февраля': 2,
    'март': 3,
    'марта': 3,
    'апрель': 4,
    'апреля': 4,
    'май': 5,
    'мая': 5,
    'июнь': 6,
    'июня': 6,
    'июль': 7,
    'июля': 7,
    'август': 8,
    'августа': 8,
    'сентябрь': 9,
    'сентября': 9,
    'октябрь': 10,
    'октября': 10,
    'ноябрь': 11,
    'ноября': 11,
    'декабрь': 12,
    'декабря': 12,
}

parser_config = {
    # 'bbc': {
    #     'link': 'https://feeds.bbci.co.uk/russian/rss.xml',
    #     'parser': BbcParser,
    #     'path': 'tables/bbc.csv',
    # },
    # 'commersant_politics': {
    #     'link': 'https://www.kommersant.ru/RSS/section-politics.xml',
    #     'parser': CommersantParser,
    #     'path': 'tables/commersant.csv',
    #     'category': 'Политика'
    # },
    # 'commersant_economics': {
    #     'link': 'https://www.kommersant.ru/RSS/section-economics.xml',
    #     'parser': CommersantParser,
    #     'path': 'tables/commersant.csv',
    #     'category': 'Экономика'
    # },
    # 'commersant_business': {
    #     'link': 'https://www.kommersant.ru/RSS/section-business.xml',
    #     'parser': CommersantParser,
    #     'path': 'tables/commersant.csv',
    #     'category': 'Бизнес'
    # },
    # 'commersant_world': {
    #     'link': 'https://www.kommersant.ru/RSS/section-world.xml',
    #     'parser': CommersantParser,
    #     'path': 'tables/commersant.csv',
    #     'category': 'Мир'
    # },
    # 'commersant_accidents': {
    #     'link': 'https://www.kommersant.ru/RSS/section-accidents.xml',
    #     'parser': CommersantParser,
    #     'path': 'tables/commersant.csv',
    #     'category': 'Происшествия'
    # },
    # 'lenta': {
    #     'link': 'https://lenta.ru/rss',
    #     'parser': LentaParser,
    #     'path': 'tables/lenta.csv'
    # },
    # 'rbc': {
    #     'link': 'http://static.feed.rbc.ru/rbc/logical/footer/news.rss',
    #     'parser': RbcParser,
    #     'path': 'tables/rbc.csv'
    # },
    # 'ria': {
    #     'link': 'https://ria.ru/export/rss2/archive/index.xml',
    #     'parser': RiaParser,
    #     'path': 'tables/ria.csv'
    # },
    # 'rt': {
    #     'link': 'https://russian.rt.com/rss',
    #     'parser': RtParser,
    #     'path': 'tables/rt.csv'
    # },
    # 'tass': {
    #     'link': 'https://tass.ru/rss/v2.xml',
    #     'parser': TassParser,
    #     'path': 'tables/tass.csv'
    # },
    # 'vedomosti': {
    #     'link': 'https://www.vedomosti.ru/rss/news',
    #     'parser': VedomostiParser,
    #     'path': 'tables/vedomosti.csv'
    # },
    # 'yandex': {
    #     'link': 'http://news.yandex.ru/index.rss',
    #     'parser': YaParser,
    #     'path': 'tables/yandex.csv'
    # }
}

PATH = '~/PycharmProjects/diploma/'

CATEGORY_MAP = {
    'Авто': 'Авто',
    'Авто / Автомобильная промышленность': 'Авто',
    'Авто / Дизайн и технологии': 'Авто',
    'Авто / Коммерческие автомобили': 'Авто',
    'Авто / Легковые автомобили': 'Авто',
    'Бизнес / Агропром': 'Бизнес',
    'Бизнес / Промышленность': 'Бизнес',
    'Бизнес / Спортивный бизнес': 'Бизнес',
    'Бизнес / ТЭК': 'Бизнес',
    'Бизнес / Торговля и услуги': 'Бизнес',
    'Бизнес / Транспорт': 'Бизнес',
    'Карьера': 'Карьера',
    'Карьера / Отставки и назначения': 'Карьера',
    'Комментарии': 'Мнения',
    'Личный счет': 'Мнения',
    'Медиа': 'Медиа',
    'Менеджмент': 'Менеджмент',
    'Менеджмент / Управление': 'Менеджмент',
    'Мнения': 'Мнения',
    'Недвижимость': 'Недвижимость',
    'Недвижимость / Жилая недвижимость': 'Недвижимость',
    'Недвижимость / Коммерческая недвижимость': 'Недвижимость',
    'Недвижимость / Стройки и инфраструктура': 'Недвижимость',
    'Общество': 'Общество',
    'Политика': 'Политика',
    'Политика / Армия и спецслужбы': 'Политика',
    'Политика / Безопасность и право': 'Политика',
    'Политика / Власть': 'Политика',
    'Политика / Демократия': 'Политика',
    'Политика / Международная жизнь': 'Политика',
    'Политика / Международные новости': 'Политика',
    'Политика / Международные отношения': 'Политика',
    'Политика / Социальная политика': 'Политика',
    'Стиль жизни': 'Стиль жизни',
    'Стиль жизни / Досуг': 'Стиль жизни',
    'Стиль жизни / Культура': 'Стиль жизни',
    'Стиль жизни / Линии жизни': 'Стиль жизни',
    'Стиль жизни / Спорт': 'Спорт',
    'Спорт': 'Спорт',
    'Технологии': 'Технологии',
    'Технологии / ИТ-бизнес': 'Технологии',
    'Технологии / Интернет и digital': 'Технологии',
    'Технологии / Медиа': 'Технологии',
    'Технологии / Наукоемкие технологии': 'Технологии',
    'Технологии / Персональные технологии': 'Технологии',
    'Технологии / Телекоммуникации': 'Технологии',
    'Финансы': 'Финансы',
    'Финансы / Банки': 'Финансы',
    'Финансы / Персональные финансы': 'Финансы',
    'Финансы / Профучастники': 'Финансы',
    'Финансы / Рынки': 'Финансы',
    'Финансы / Страхование': 'Финансы',
    'Экономика': 'Экономика',
    'Экономика / Госинвестиции и проекты': 'Экономика',
    'Экономика / Макроэкономика и бюджет ': 'Экономика',
    'Экономика / Мировая экономика': 'Экономика',
    'Экономика / Налоги и сборы': 'Экономика',
    'Экономика / Правила': 'Экономика'
}

keywords = {
    'экономика': 'экономика',
    'экономист': 'экономика',
    'ввп': 'экономика',
    'центробанк': 'экономика',
    'тренер': 'Спорт',
    'сыграл': 'Спорт',
    'вничью': 'Спорт',
    'спринт': 'Спорт',
    'футбол': 'Спорт',
    'футболист': 'Спорт',
    'хоккеист': 'Спорт',
    'лыжник': 'Спорт',
    'лыжница': 'Спорт',
    'фифа': 'Спорт',
    'уефа': 'Спорт',
    'бокс': 'Спорт',
    'сборная': 'Спорт',
    'золото': 'Спорт',
    'бронза': 'Спорт',
    'медаль': 'Спорт',
    'президент': 'Политика',
    'министр': 'Политика',
    'закон': 'Политика',
    'эрдоган': 'Политика',
    'зеленский': 'Политика',
    'украина': 'Политика',
    'сша': 'Политика',
    'меркель': 'Политика',
    'песков': 'Политика',
    'канцлер': 'Политика',
    'макрон': 'Политика',
    'афганистан': 'Политика',
    'талибы': 'Политика',
    'трамп': 'Политика',
    'путин': 'Политика',
    'депутат': 'Политика',
    'госдума': 'Политика',
    'сенатор': 'Политика',
    'мид': 'Политика',
    'парламент': 'Политика',
    'митинг': 'Происшествия',
    'дтп': 'Происшествия',
    'снег': 'Происшествия',
    'похолодание': 'Происшествия',
    'спасатели': 'Происшествия',
    'град': 'Происшествия',
    'пожар': 'Происшествия',
    'взрыв': 'Происшествия',
    'теракт': 'Происшествия',
    'погибли': 'Происшествия',
    'погиб': 'Происшествия',
    'доллар': 'экономика',
    'евро': 'экономика',
    'валюта': 'экономика',
}

events = {
    'Олимпиада в Пхенчхане': {
        'words': ['олимпиада', 'сборная', 'мок'],
        'column_name': 'is_olymp',
        'start_date': datetime.datetime(2018, 2, 9),
        'end_date': datetime.datetime(2018, 2, 26)
    },
    'Чемпионат мира по футболу, 2018': {
        'words': ['футбол', 'сборная', 'футболист', 'матч', 'фифа'],
        'column_name': 'is_wch',
        'start_date': datetime.datetime(2018, 6, 14),
        'end_date': datetime.datetime(2018, 7, 16)
    },
    'Выборы Президента': {
        'words': ['выборы', 'грудинин', 'цик', 'памфилова', 'путин', 'президент',
                  'фальсификации', 'наблюдатели', 'навальный', 'голосование', 'умное'],
        'column_name': 'president_elections',
        'start_date': datetime.datetime(2018, 3, 16),
        'end_date': datetime.datetime(2018, 3, 20)
    },
    'Пожар в зимней вишне': {
        'words': ['пожар', 'зимняя', 'вишня', 'кемерово', 'кемеровский', 'тц'],
        'column_name': 'is_fire',
        'start_date': datetime.datetime(2018, 3, 25),
        'end_date': datetime.datetime(2018, 3, 28)
    },
    'Стрельба в Керчи': {
        'words': ['керчь', 'керченский', 'стрелок', 'политехнический', 'колледж', 'ск', 'скр', 'теракт'],
        'column_name': 'is_kerch',
        'start_date': datetime.datetime(2018, 10, 17),
        'end_date': datetime.datetime(2018, 10, 21)
    },
    'Катастрофа SSJ в Шереметьево': {
        'words': ['ssj', 'суперджет', 'шереметьево', 'катастрофа', 'аэрофлот', 'аврийный', 'ящик'],
        'column_name': 'ssj',
        'start_date': datetime.datetime(2019, 5, 5),
        'end_date': datetime.datetime(2019, 5, 8)
    },
    'Отравление Навального': {
        'words': ['навальный', 'новичок', 'отравлен', 'кома', 'политик',
                  'оппозиционер', 'германия', 'берлин', 'омск', 'фбк'],
        'column_name': 'ssj',
        'start_date': datetime.datetime(2020, 8, 20),
        'end_date': datetime.datetime(2020, 8, 23)
    },
    'Олимпиада в Токио': {
        'words': ['олимпиада', 'сборная', 'мок'],
        'column_name': 'is_olymp',
        'start_date': datetime.datetime(2021, 7, 22),
        'end_date': datetime.datetime(2021, 8, 9)
    },
    'Выборы в ГосДуму': {
        'words': ['выборы', 'голосование', 'депутат', 'госдума', 'цик', 'памфилова',
                  'фальсификации', 'дэг', 'кпрф', 'едро', 'единая', 'лдпр', 'фбк', 'навальный'],
        'column_name': '2021_elections',
        'start_date': datetime.datetime(2021, 9, 16),
        'end_date': datetime.datetime(2021, 9, 19)
    }
}

NUM_CLUSTERS = 20

main_config = {
    'vectorizer': RuBertVectorizer,
    'clusterizer': AgglomerativeClusterizer,
    'distancer': CentroidDistancer,
    'connector': MLEConnector,
}

vectorizers = [['Vec: RuBert', RuBertVectorizer], ['Vec: TfIdf', ComplexTfIdfVectorizer]]
clusterizers = [['Clu: Bert', BertBasedClusterizer], ['Clu: Agg,', AgglomerativeClusterizer]]
distancers = [['Dist: Centr,', CentroidDistancer]]
connectors = [['Con: GrUsed,', GreedyConnectorWithUsed], ['Con: MLE,', MLEConnector], ['Con: Greed,', GreedyConnector]]

df_name = 'manual_df'
