import datetime
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import config
import utils

from collector import Collector
from scorer import Scorer
from tqdm.auto import tqdm

import warnings
warnings.filterwarnings('ignore')


def get_matching(df, emb_df, clusterizer, distancer, connector,
                 start_date=datetime.datetime(2018, 1, 1),
                 end_date=datetime.datetime(2022, 1, 1),
                 period=datetime.timedelta(days=1),
                 shift=datetime.timedelta(hours=3), num_clusters=20, use_prev=True, num_epochs=5):
    cur_date = start_date
    delta = (end_date - cur_date) / shift

    df = df[(df.loc[:, 'timestamp'] < end_date) & (df.loc[:, 'timestamp'] >= start_date)]
    df.reset_index(inplace=True)
    del df['index']
    emb_df = emb_df[(emb_df.loc[:, 'timestamp'] < end_date) & (emb_df.loc[:, 'timestamp'] >= start_date)]
    emb_df.reset_index(inplace=True)
    del emb_df['index']

    main_dict = {'date': [], 'matrix': [], 'probs': [], 'match': [],
                 'indices': [], 'labels': [], 'num_clusters': []}
    prev_n_clusters = -1
    emb_df.describe()
    try:
        emb_df.loc[:, 'timestamp'] = emb_df.loc[:, 'timestamp'].apply(lambda x: datetime.datetime.fromtimestamp(x))
    except:
        pass
    try:
        df.loc[:, 'timestamp'] = df.loc[:, 'timestamp'].apply(lambda x: datetime.datetime.fromtimestamp(x))
    except:
        pass

    prev_emb = None
    for i in tqdm(range(int(delta))):
        group = df[(df.loc[:, 'timestamp'] < cur_date + period) &
                   (df.loc[:, 'timestamp'] >= cur_date)].drop('timestamp', axis=1)
        emb_group = emb_df[(emb_df.loc[:, 'timestamp'] < cur_date + period) &
                           (emb_df.loc[:, 'timestamp'] >= cur_date)].drop('timestamp', axis=1)
        main_dict['indices'].append(emb_group.index.values)

        cur_clusterizer = clusterizer(num_clusters, in_size=emb_group.shape[1])
        if i > 0 and use_prev:
            n_clusters, labels = cur_clusterizer.fit_predict(group, emb_group, prev_emb=prev_emb)
        else:
            n_clusters, labels = cur_clusterizer.fit_predict(group, emb_group)
        main_dict['labels'].append(labels)
        main_dict['num_clusters'].append(n_clusters)
        emb_group['label'] = labels

        if i > 0:
            prev_matr = main_dict['matrix'][-1]
            d = distancer(prev_n_clusters, n_clusters, prev_matr, emb_group)
            distances = d.calculate()
            matcher = connector(distances)
            main_dict['probs'].append(distances)
            matching = matcher.calculate()
            main_dict['match'].append(matching)

        main_dict['date'].append(cur_date)
        main_dict['matrix'].append(emb_group)
        cur_date += shift
        prev_n_clusters = n_clusters
        prev_emb = emb_group

    return main_dict


def experiment(df, vectorizer, clusterizer, distancer, connector, read_from_csv, name_file):
    vec = vectorizer()
    if read_from_csv:
        emb_df = pd.read_csv(name_file)
        emb_df['timestamp'] = pd.to_datetime(emb_df['timestamp'])
        emb_df['timestamp'] = df['timestamp']
        del emb_df['Unnamed: 0']
        emb_df.reset_index(inplace=True)
        del emb_df['index']
    else:
        emb_df = vec.fit_transform(df)
        emb_df['timestamp'] = df['timestamp']
        emb_df.to_csv(name_file)
    scorer = Scorer(df)
    for num_clusters in [2, 3, 4, 5, 6, 8, 10, 12]:
        res = get_matching(df, emb_df, clusterizer, distancer, connector, num_clusters=num_clusters)
        acc = scorer.calculate(res)
        print(f'У {num_clusters} кластеров точность: {acc * 100:.2f}%')


df = pd.read_csv(config.PATH + config.df_name + '.csv')
df = utils.make_data_pretty(df)
print("Известна категория:", df.shape[0] - np.sum(df['category'].isna()), "\nВсего:", df.shape[0])

df.loc[:, 'timestamp'] = df.loc[:, 'timestamp'].apply(lambda x: datetime.datetime.fromtimestamp(x))
# df = df[df.loc[:, 'timestamp'] >= datetime.datetime(2021, 6, 1)]
df.reset_index(inplace=True)
del df['index']

for vectorizer in config.vectorizers:
    read_from_csv = True
    name_file = 'emb_' + config.df_name + vectorizer[0][5:] + '.csv'
    for clusterizer in config.clusterizers:
        for distancer in config.distancers:
            for connector in config.connectors:
                print("Current model:", vectorizer[0], clusterizer[0], distancer[0], connector[0])
                experiment(df, vectorizer[1], clusterizer[1], distancer[1], connector[1], read_from_csv, name_file)
                read_from_csv = True
