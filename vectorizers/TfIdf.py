from vectorizers.abstract import AbstractVectorizer

import numpy as np
import datetime
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy.sparse import csr_matrix


class ComplexTfIdfVectorizer(AbstractVectorizer):
    def __init__(self):
        super().__init__()
        self.vec = TfidfVectorizer(max_df=0.98, min_df=0.01)

    def fit_transform(self, df, use_text=False):
        title_matrix = csr_matrix.toarray(self.vec.fit_transform(df['title'].to_list()))
        if use_text:
            text_matrix = csr_matrix.toarray(self.vec.fit_transform(df['text'].to_list()))
        timestamp_matrix = np.array(df['timestamp'])[..., None]
        timestamp_matrix = timestamp_matrix.astype(int)
        num_columns = 1 + title_matrix.shape[1]
        if use_text:
            num_columns += text_matrix.shape[1]
            stacked_array = (np.c_[timestamp_matrix, title_matrix, text_matrix])
        else:
            stacked_array = (np.c_[timestamp_matrix, title_matrix])
        emb_df = pd.DataFrame(stacked_array, columns=['timestamp'] + [str(i) for i in range(1, num_columns)])
        emb_df.iloc[:, 0] = emb_df.iloc[:, 0].apply(lambda x: datetime.datetime.fromtimestamp(int(x / 1e9)))
        emb_df.sort_values(by=['timestamp'], inplace=True)
        return emb_df
