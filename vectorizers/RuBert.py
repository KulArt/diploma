from vectorizers.abstract import AbstractVectorizer

import numpy as np
import datetime
import pandas as pd

import torch
from transformers import BertModel, BertTokenizer

from tqdm.auto import tqdm
from transformers import logging

logging.set_verbosity(40)


class RuBertVectorizer(AbstractVectorizer):
    def __init__(self):
        super().__init__()
        self.tokenizer = BertTokenizer.from_pretrained('DeepPavlov/rubert-base-cased-conversational')
        self.model = BertModel.from_pretrained('DeepPavlov/rubert-base-cased-conversational', output_hidden_states=True)
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.model = self.model.to(self.device)
        self.model.eval()

    def fit_transform(self, df, use_text=False):
        embedded_titles = []
        for title in df['title']:
            title_ids = torch.LongTensor(self.tokenizer.encode(title))
            title_ids = title_ids.to(self.device)
            title_ids = title_ids.unsqueeze(0)
            with torch.no_grad():
                out = self.model(input_ids=title_ids)
            hidden_states = out[2]
            title_embedding = torch.mean(hidden_states[-1], dim=1).squeeze()
            embedded_titles.append(title_embedding.numpy())

        title_matrix = np.array(embedded_titles)
        if use_text:
            embedded_texts = []
            for text in df['text']:
                text_ids = torch.LongTensor(self.tokenizer.encode(text))
                text_ids = text_ids.to(self.device)
                text_ids = text_ids.unsqueeze(0)
                with torch.no_grad():
                    out = self.model(input_ids=text_ids)
                hidden_states = out[2]
                text_embedding = torch.mean(hidden_states[-1], dim=1).squeeze()
                embedded_texts.append(text_embedding)
            text_matrix = np.array(embedded_texts)

        timestamp_matrix = np.array(df['timestamp'])[..., None]
        timestamp_matrix = timestamp_matrix.astype(int)
        num_columns = 1 + title_matrix.shape[1]
        if use_text:
            num_columns += text_matrix.shape[1]
            stacked_array = (np.c_[timestamp_matrix, title_matrix, text_matrix])
        else:
            stacked_array = (np.c_[timestamp_matrix, title_matrix])
        emb_df = pd.DataFrame(stacked_array, columns=['timestamp'] + [str(i) for i in range(1, num_columns)])
        emb_df.iloc[:, 0] = emb_df.iloc[:, 0].apply(lambda x: datetime.datetime.fromtimestamp(int(x / 1e9)))
        emb_df.sort_values(by=['timestamp'], inplace=True)
        return emb_df
