from abc import abstractmethod


class AbstractVectorizer:
    def __init__(self):
        ...

    @abstractmethod
    def fit_transform(self, df):
        ...
