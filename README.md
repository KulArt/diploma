# Dynamic clustering model

Model for dynamic online clustering of texts. Creates time series, generating an acyclic time-dependency graph.

The online mode means that data can be given to model as a stream. The dynamic one is about new batch of data can be given wuthout necessarity to re-learn

The model consists of several modules:
* Collector - module for collecting data. In this case, news.
* Vectorizer - module for getting embeddings.
* Cluserizer - module for clustering data.
* Distancer - module for calculating distances between clusters. Further it casts to "probabilities" via softmax
* Connector or matcher - module for building time dependencies between consecutive timed series of groups of clusters

### Collector

It is easy class which use parsers to get data about news from RSS format on webarchive.

### Vectorizer

1. TfIdf approach

2. DeepPavlov's Bert

### Clusterizer

1. Thematic modeling eg. LDA

2. Agglomerative clustering

3. As we have clusters for previous layer of objects, let's use classifier (in project it's tiny neural fc-network) to generate probabilities for generating probaility space for each object to each group and cluster it in this space.

### Distancer

Distance between centroid of clusters.

### Connector

1. Greedy

2. Greedy with unique matching

3. MLE by finding min cost perfect matching

## Results

Combiation of Bert and FC-network as clustering model is the best one. Accuracy of static and dynamic clustering are more than 75%.
