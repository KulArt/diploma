from config import *
from nltk.stem.snowball import SnowballStemmer
import numpy as np

from tqdm.auto import tqdm


def int_value_from_ru_month(date_str):
    for k, v in RU_MONTH_VALUES.items():
        date_str = date_str.replace(k, str(v))
    return date_str


def make_data_pretty(data):
    del data['Unnamed: 0']
    data.dropna(inplace=True, subset=['title', 'timestamp'])

    data['text'] = data['text'].astype(str)
    data['title'] = data['title'].astype(str)
    data['timestamp'] = data['timestamp'].astype(int)

    data['title'] = data['title'].apply(lambda x: x.lower().strip())
    data.drop_duplicates(subset=['title'], inplace=True)

    data.loc[data['text'].isna(), 'text'] = '<UNK>'
    data.sort_values(by=['timestamp'], inplace=True)
    data.reset_index(drop=True, inplace=True)
    return data


def fill_category_for_word(data, word, category):
    snow_stemmer = SnowballStemmer(language='russian')
    stem_word = snow_stemmer.stem(word)
    for idx, row in data.iterrows():
        try:
            if stem_word in row['fixed_title'].split() and np.isnan(row['category']):
                data.loc[idx, 'category'] = category
        except:
            continue


def fill_categories(data):
    df = data.copy()
    for k, v in tqdm(df.items()):
        fill_category_for_word(df, k, v)
    return data
