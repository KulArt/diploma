import numpy as np
import pandas as pd

from sklearn.metrics import f1_score

from config import CATEGORY_MAP


class Scorer:
    def __init__(self, df):
        self.df = df

    def calculate(self, main_dict):
        number_layers = len(main_dict['num_clusters'])
        layer_score = []
        for layer_num in range(number_layers - 1):
            layer_score += [0.]
            left_indices = main_dict['indices'][layer_num]
            right_indices = main_dict['indices'][layer_num + 1]
            if len(right_indices) == 0 or len(left_indices) == 0:
                continue
            left_labels = main_dict['labels'][layer_num]
            right_labels = main_dict['labels'][layer_num + 1]
            layer_not_nan_size = 0
            for target_label in range(main_dict['num_clusters'][layer_num]):
                try:
                    source_label = main_dict['match'][layer_num][target_label]
                except:
                    if len(layer_score) > 0:
                        layer_score.pop()
                    continue

                target_indices = [i for i, x in enumerate(right_labels) if x == target_label]
                source_indices = [i for i, x in enumerate(left_labels) if x == source_label]

                left_df = self.df.iloc[left_indices[source_indices]]
                right_df = self.df.iloc[right_indices[target_indices]]

                left_category = left_df[~left_df['category'].isna()]['category'].copy()
                right_category = right_df[~right_df['category'].isna()]['category'].copy()

                if left_category.shape[0] == 0 or right_category.shape[0] == 0:
                    continue

                left_majority = self.__get_major_category__(left_category.to_list())
                right_majority = self.__get_major_category__(right_category.to_list())
                layer_not_nan_size += 1

                layer_score[-1] += (left_majority == right_majority)
            if len(layer_score) == 0:
                continue
            if layer_not_nan_size != 0:
                layer_score[-1] /= layer_not_nan_size
            else:
                if len(layer_score) > 0:
                    layer_score.pop()
        return np.mean(layer_score)

    @staticmethod
    def __get_major_category__(categories_list):
        occurences = {}
        for i, item in enumerate(categories_list):
            if item in CATEGORY_MAP.keys():
                categories_list[i] = CATEGORY_MAP[item]
            if categories_list[i] in occurences.keys():
                occurences[categories_list[i]] += 1
            else:
                occurences[categories_list[i]] = 1
        max_val = -1
        res = ''
        for k, v in occurences.items():
            if v > max_val:
                res = k
        return res
