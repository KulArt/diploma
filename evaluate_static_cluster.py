import pandas as pd
import numpy as np
import datetime
import config
import utils
import warnings

import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from pprint import pprint
from tqdm.auto import tqdm

warnings.filterwarnings('ignore')
nltk.download('wordnet')
nltk.download('stopwords')
nltk.download('omw-1.4')


def clean(text):
    snow_stemmer = SnowballStemmer(language='russian')
    swords = set(stopwords.words('russian'))
    wordnet_lemmatizer = WordNetLemmatizer()
    tokens = nltk.tokenize.word_tokenize(text.lower())
    tokens = [t for t in tokens if t.isalpha()]
    tokens = [wordnet_lemmatizer.lemmatize(t) for t in tokens]
    tokens = [t for t in tokens if len(t) > 2]
    tokens = [t for t in tokens if t not in swords]
    tokens = [snow_stemmer.stem(t) for t in tokens]
    return ' '.join(tokens)


def get_df_for_event(data, words, column_name, start_date, end_date):
    res_df = data[(pd.to_datetime(data['timestamp'] * 1e9) >= start_date) &
                  (pd.to_datetime(data['timestamp'] * 1e9) <= end_date)]
    res_df[column_name] = False
    snow_stemmer = SnowballStemmer(language='russian')
    for idx, row in res_df.iterrows():
        for word in words:
            stem_word = snow_stemmer.stem(word)
            if stem_word in row['fixed_title'].split():
                res_df.loc[idx, column_name] = True
    return res_df


def get_accuracy_for_clustering(data, column_name, vec=config.main_config['vectorizer'](), n_clusters=20):
    data = data.copy()
    data['timestamp'] = pd.to_datetime(data['timestamp'] * 1e9)
    cur_date = np.min(data['timestamp'])
    end_date = np.max(data['timestamp'])
    acc = []

    period = datetime.timedelta(days=1)
    delta = (end_date - cur_date) / period
    for i in range(int(delta)):
        cur_df = data[(data.loc[:, 'timestamp'] < cur_date + period) &
                      (data.loc[:, 'timestamp'] >= cur_date)]
        if cur_df.shape[0] == 0:
            continue
        emb_df = vec.fit_transform(cur_df)
        del emb_df['timestamp']
        cur_df = cur_df[['title', column_name]]
        cur_clusterizer = config.main_config['clusterizer'](n_clusters)
        n_clusters, labels = cur_clusterizer.fit_predict(cur_df, emb_df)
        cur_df['label'] = labels
        gr_df = cur_df.groupby('label').sum(column_name)
        gr_df.reset_index(inplace=True)
        if np.sum(gr_df[column_name]) != 0:
            score = gr_df.iloc[gr_df.idxmax()[column_name]][column_name] / np.sum(gr_df[column_name])
            acc.append(score)
        cur_date += period

    return acc, np.mean(acc)


def get_dict_score(data, vec=config.main_config['vectorizer']()):
    res_dict = {}
    for num_clusters in tqdm([2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16]):
        mean_acc = []
        for k, v in tqdm(config.events.items()):
            l, m = get_accuracy_for_clustering(get_df_for_event(data, **v), v['column_name'], n_clusters=num_clusters, vec=vec)
            mean_acc.append(m)
            if k in res_dict.keys():
                res_dict[k] += [np.round(m * 100, 2)]
            else:
                res_dict[k] = [np.round(m * 100, 2)]
        print(f'Для {num_clusters} кластеров средняя точность: {np.mean(mean_acc) * 100:.2f}')
        if 'total' in res_dict.keys():
            res_dict['total'] += [np.round(np.mean(mean_acc) * 100, 2)]
        else:
            res_dict['total'] = [np.round(np.mean(mean_acc) * 100, 2)]
    return res_dict


df = pd.read_csv('tables/data.csv')
df = utils.make_data_pretty(df)
df['fixed_title'] = df['title'].copy().apply(clean)

pprint(get_dict_score(data=df))
pprint(get_dict_score(data=df, vec=config.vectorizers[1][1]()))
