import datetime
import collections
import pandas as pd
from time import sleep
from tqdm import tqdm
import requests
from bs4 import BeautifulSoup
import config


class Collector:
    def __collaborate__(self, old_path, new):
        try:
            old = pd.read_csv(old_path)
            print(' ')
            print(old_path, old.shape)
            del old['Unnamed: 0']
            old = pd.concat([old, new])
            old.drop_duplicates(subset=['title'], inplace=True)
            old['timestamp'] = old['timestamp'].astype(int)
            old.sort_values(by=['timestamp'])
            old.to_csv(old_path)
        except:
            new.to_csv(old_path)

    def __parse_history__(self, link, parser, dates_str, source, path, category=None):
        queue = collections.deque()
        length = 0
        i = 0
        try:
            for date_str in tqdm(dates_str):
                url = 'https://web.archive.org/web/' + date_str + '/' + link
                new_parser = parser(url, source)
                new = new_parser.parse()
                if category is not None:
                    new['category'] = category
                queue.append(new)
                length += 1
                i += 1
                sleep(0.2)
        except:
            self.__parse_history__(link, parser, dates_str[i:], source, path)
        while length > 1:
            left = queue.pop()
            right = queue.pop()
            queue.appendleft(pd.concat([left, right]))
            length -= 1

        df = queue.pop()
        df.drop_duplicates(subset=['title'], inplace=True)
        print(source, df.shape)
        try:
            df['timestamp'] = df['timestamp'].astype(int)
            df.sort_values(by=['timestamp'])
        except:
            pass
        try:
            old = pd.read_csv(path)
            df = pd.concat([old, df])
        except:
            pass
        df.to_csv(path)
        df = pd.read_csv(path)
        try:
            del df['Unnamed: 0']
            for c in range(10):
                try:
                    del df['Unnamed: 0.' + str(c)]
                except:
                    pass
        except:
            pass
        df.to_csv(path)

    def collect(self, start_date=datetime.datetime(2018, 1, 1), finish_date=datetime.datetime.now(),
                path='tables/data.csv'):
        date = start_date
        dates_str = []
        while date < finish_date:
            dates_str.append(date.strftime("%Y%m%d") + "000000")
            date += datetime.timedelta(days=1)

        df = pd.DataFrame.from_dict({'timestamp': [], 'source': [], 'title': [],
                                     'text': [], 'link': [], 'category': []})

        for k, v in tqdm(config.parser_config.items()):
            if 'category' in v.keys():
                self.__parse_history__(v['link'], v['parser'], dates_str, k, config.PATH + v['path'], v['category'])
            else:
                self.__parse_history__(v['link'], v['parser'], dates_str, k, config.PATH + v['path'])
            tmp = pd.read_csv(config.PATH + v['path'])
            for column in tmp.columns:
                if column[:7] == 'Unnamed':
                    del tmp[column]
            tmp.to_csv(config.PATH + v['path'])
            df = pd.concat([df, tmp])

        for column in df.columns:
            if column[:7] == 'Unnamed':
                del df[column]

        df.sort_values(by=['timestamp'])
        df.to_csv(config.PATH + path)
