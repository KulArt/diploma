import numpy as np
import pandas as pd

from connectors.abstract import AbstractConnector


class MLEConnector(AbstractConnector):
    def __init__(self, distances, with_fictive=False):
        super().__init__(distances)
        self.with_fictive = with_fictive
        self.is_reversed = False

    def calculate(self):
        self.__precalc__()
        num_clusters_in_cur_layer = self.distances.shape[0] - 1
        num_clusters_in_next_layer = self.distances[0].shape[0] - 1
        u_potential = np.zeros((num_clusters_in_cur_layer + 1))
        v_potential = np.zeros((num_clusters_in_next_layer + 1))
        permutation = np.zeros((num_clusters_in_next_layer + 1), dtype=np.int)
        augment_chain = np.zeros((num_clusters_in_next_layer + 1), dtype=np.int)
        for cluster_in_cur in range(1, num_clusters_in_cur_layer + 1):
            permutation[0] = cluster_in_cur
            free_cluster_in_next = 0
            min_v_potential = [1e9] * (num_clusters_in_next_layer + 1)
            used = [False] * (num_clusters_in_next_layer + 1)
            while True:
                used[free_cluster_in_next] = True
                i0 = int(permutation[free_cluster_in_next])
                delta = np.inf
                argmin_potential_in_next = 0
                for cluster_in_next in range(1, num_clusters_in_next_layer + 1):
                    if not used[cluster_in_next]:
                        cur_potential = self.distances[i0][cluster_in_next] - \
                                        u_potential[i0] - v_potential[cluster_in_next]
                        if cur_potential < min_v_potential[cluster_in_next]:
                            min_v_potential[cluster_in_next] = cur_potential
                            augment_chain[cluster_in_next] = free_cluster_in_next
                        if min_v_potential[cluster_in_next] < delta:
                            delta = min_v_potential[cluster_in_next]
                            argmin_potential_in_next = cluster_in_next

                for cluster_in_next in range(num_clusters_in_next_layer + 1):
                    if used[cluster_in_next]:
                        u_potential[permutation[cluster_in_next]] += delta
                        v_potential[cluster_in_next] -= delta
                    else:
                        min_v_potential[cluster_in_next] -= delta

                free_cluster_in_next = argmin_potential_in_next
                if permutation[free_cluster_in_next] == 0:
                    break

            while True:
                argmin_potential_in_next = augment_chain[free_cluster_in_next]
                permutation[int(free_cluster_in_next)] = permutation[argmin_potential_in_next]
                free_cluster_in_next = argmin_potential_in_next
                if free_cluster_in_next == 0:
                    break

        if self.is_reversed:
            return permutation[1:] - 1

        matching = np.zeros((num_clusters_in_cur_layer + 1)) - 1
        for cluster_in_next in range(1, num_clusters_in_next_layer + 1):
            matching[permutation[cluster_in_next]] = cluster_in_next
        return matching[1:] - 1

    def __precalc__(self):
        new_dist = np.zeros((self.distances.shape[0]+1, 
                             self.distances.shape[1]+1))
        new_dist[1:, 1:] = self.distances
        new_dist[0, :] += 1
        new_dist[:, 0] += 1
        new_dist = -np.log(new_dist)
        self.distances = new_dist
        if self.distances.shape[0] > self.distances.shape[1]:
            self.distances = self.distances.T
            self.is_reversed = True

