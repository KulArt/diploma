from abc import abstractmethod


class AbstractConnector:
    def __init__(self, distances):
        self.distances = distances

    def calculate(self):
        ...
