import numpy as np
import pandas as pd

from connectors.abstract import AbstractConnector


class GreedyConnector(AbstractConnector):
    def __init__(self, distances):
        super().__init__(distances)

    def calculate(self):
        matching = [-1] * self.distances[0].shape[0]
        for source in range(self.distances[0].shape[0]):
            best_target = -1
            best_dist = np.inf
            for target in range(self.distances.shape[0]):
                if self.distances[target][source] < best_dist:
                    best_target = target
                    best_dist = self.distances[target][source]
            try:
                matching[source] = best_target
            except:
                print(source, len(matching), self.distances[0].shape[0])
        return matching
