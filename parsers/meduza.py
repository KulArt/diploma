import requests
from bs4 import BeautifulSoup

import sys
import datetime
import re
from time import sleep
from tqdm import tqdm

import pandas as pd

from utils import *


class MeduzaParser:
    def __init__(self):
        self.url = 'https://meduza.io/'
        self.init_tag = 'a'
        self.init_attrs = {'class': 'Link-root Link-isInBlockTitle'}
        try:
            self.page = requests.get(self.url)
            soup = BeautifulSoup(self.page.text, "html.parser")
            self.links = soup.find_all(self.init_tag, self.init_attrs)

            self.news_links = []
            for link in self.links:
                link = link['href']
                if link[:5] == "/news":
                    self.news_links.append(link)

        except Exception as e:
            error_type, error_obj, error_info = sys.exc_info()
            print('ERROR FOR LINK:', self.url)
            print(error_type, 'Line:', error_info.tb_lineno)
        self.content = {'timestamp': [], 'source': [], 'title': [], 'text': [], 'link': []}

    def __parse_note__(self, link):
        cur_link = self.url + link
        self.content['link'].append(cur_link)
        self.content['source'].append('meduza')
        try:
            page = requests.get(cur_link)
            cur_soup = BeautifulSoup(page.text, "html.parser")
            cur_soup.prettify(formatter=lambda s: s.replace(u'\xa0', ' '))
            title = cur_soup.title.string[:-9]
            self.content['title'].append(title)

            date = str(cur_soup.find_all('time')[0]).split('>')[1].split('<')[0]
            date_str = int_value_from_ru_month(date)
            timestamp = datetime.datetime.strptime(date_str, '%H:%M, %d %m %Y').timestamp()
            self.content['timestamp'].append(timestamp)

            text = cur_soup.find_all('p', attrs={
                'class': ['SimpleBlock-module_p__Q3azD', 'SimpleBlock-module_context_p__33saY']})
            clean = []
            for par in text:
                par = str(par).replace('&nbsp;', ' ')
                par = re.sub(r"<[^>]*>", "", par)
                clean.append(par)
            text = "".join(clean)
            self.content['text'].append(text)
        except Exception as e:
            print(e)
            error_type, error_obj, error_info = sys.exc_info()
            print('ERROR FOR LINK:', cur_link)
            print(error_type, 'Line:', error_info.tb_lineno)

        sleep(0.5)

    def parse(self):
        for link in tqdm(self.news_links):
            self.__parse_note__(link)
        return pd.DataFrame.from_dict(self.content)
