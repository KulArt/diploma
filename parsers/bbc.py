import pandas as pd
import requests
from bs4 import BeautifulSoup

import sys
import datetime


class BbcParser:
    def __init__(self, url, source=''):
        self.url = url
        self.source = source
        try:
            self.page = requests.get(self.url)
            self.soup = BeautifulSoup(self.page.content, "html.parser")
        except:
            error_type, error_obj, error_info = sys.exc_info()
            print('ERROR FOR LINK:', self.url)
            print(error_type, 'Line:', error_info.tb_lineno)

    def __get_links__(self):
        tag = 'guid'
        unformated_links = self.soup.find_all(tag)
        links = [str(s)[len(tag) + 30:-(len(tag) + 6)].strip() for s in unformated_links]
        return links

    def __get_dates__(self):
        tag = 'pubdate'
        unformated_dates = self.soup.find_all(tag)
        dates = [datetime.datetime.strptime(str(s)[len(tag) + 2:-(len(tag) + 3)][5:-4], '%d %b %Y %H:%M:%S')
                                            .timestamp() for s in unformated_dates]
        return dates

    def __get_titles__(self):
        tag = 'title'
        unformated_titles = self.soup.find_all(tag)
        titles = [str(s)[len(tag) + 11:-(len(tag) + 6)].strip() for s in unformated_titles][2:]
        return titles

    def __get_content__(self):
        tag = 'description'
        unformated_contents = self.soup.find_all(tag)
        contents = [str(s)[len(tag) + 1:-(len(tag) + 3)][8:-4] for s in unformated_contents[1:]]
        return contents

    def parse(self):
        df = {'timestamp': self.__get_dates__(),
              'source': [self.source] * len(self.__get_dates__()),
              'title': self.__get_titles__(),
              'text': self.__get_content__(),
              'link': [None] * len(self.__get_dates__())}
        return pd.DataFrame.from_dict(df)
