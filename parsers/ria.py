import pandas as pd
import requests
from bs4 import BeautifulSoup

import sys
import datetime


class RiaParser:
    def __init__(self, url, source=''):
        self.url = url
        self.source = source
        try:
            self.page = requests.get(self.url)
            self.soup = BeautifulSoup(self.page.text, "html.parser")
        except:
            error_type, error_obj, error_info = sys.exc_info()
            print('ERROR FOR LINK:', self.url)
            print(error_type, 'Line:', error_info.tb_lineno)

    def __get_links__(self):
        tag = 'guid'
        unformated_links = self.soup.find_all(tag)
        links = [str(s)[len(tag) + 2:-(len(tag) + 3)] for s in unformated_links]
        return links

    def __get_dates__(self):
        tag = 'pubdate'
        unformated_dates = self.soup.find_all(tag)
        dates = [datetime.datetime.strptime(str(s)[len(tag) + 2:-(len(tag) + 3)][5:-6], '%d %b %Y %H:%M:%S')
                                            .timestamp() for s in unformated_dates]
        return dates

    def __get_titles__(self):
        tag = 'title'
        unformated_titles = self.soup.find_all(tag)
        titles = [str(s)[len(tag) + 2:-(len(tag) + 3)] for s in unformated_titles][2:]
        return titles

    def __get_content__(self):
        return []

    def parse(self):
        df = {'timestamp': self.__get_dates__(),
              'source': [self.source] * len(self.__get_dates__()),
              'title': self.__get_titles__(),
              'text': self.__get_content__(),
              'link': self.__get_links__(),
              'category': [None] * len(self.__get_dates__())}
        max_len = 0
        for k, v in df.items():
            max_len = max(max_len, len(v))
        for k in df.keys():
            df[k] += [None] * (max_len - len(df[k]))
        return pd.DataFrame.from_dict(df)
