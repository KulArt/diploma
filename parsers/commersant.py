import pandas as pd
import requests
from bs4 import BeautifulSoup

import sys
import datetime
from time import sleep


class CommersantParser:
    def __init__(self, url, source=''):
        self.url = url
        self.source = source
        try:
            self.page = requests.get(url)
            self.soup = BeautifulSoup(self.page.content, "html.parser")
        except:
            error_type, error_obj, error_info = sys.exc_info()
            print('ERROR FOR LINK:', url)
            print(error_type, 'Line:', error_info.tb_lineno)


    def __get_links__(self, soup):
        tag = 'guid'
        unformated_links = soup.find_all(tag)
        links = [str(s)[len(tag) + 11:-(len(tag) + 6)] for s in unformated_links]
        return links

    def __get_dates__(self, soup):
        tag = 'pubdate'
        unformated_dates = soup.find_all(tag)
        dates = [datetime.datetime.strptime(str(s)[len(tag) + 2:-(len(tag) + 3)][5:-6], '%d %b %Y %H:%M:%S')
                                            .timestamp() for s in unformated_dates]
        return dates

    def __get_titles__(self, soup):
        tag = 'title'
        unformated_titles = soup.find_all(tag)
        titles = [str(s)[len(tag) + 2:-(len(tag) + 3)] for s in unformated_titles][2:]
        return titles

    def __get_content__(self, soup):
        tag = 'description'
        unformated_contents = soup.find_all(tag)
        contents = [str(s)[len(tag) + 2:-(len(tag) + 3)][10:-4] for s in unformated_contents][1:]
        return contents

    def __get_category__(self, soup):
        tag = 'category'
        unformated_contents = soup.find_all(tag)
        contents = [str(s)[len(tag) + 2:-(len(tag) + 3)][10:-4] for s in unformated_contents][1:]
        return contents

    def parse(self):
        df = {'timestamp': self.__get_dates__(self.soup),
              'source': [self.source] * len(self.__get_dates__(self.soup)),
              'title': self.__get_titles__(self.soup),
              'text': [None] * len(self.__get_dates__(self.soup)),
              'link': self.__get_links__(self.soup),
              'category': self.__get_category__(self.soup)}
        return pd.DataFrame.from_dict(df)
