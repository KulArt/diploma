from abc import abstractmethod

import numpy as np
import pandas as pd


class AbstractDistancer:
    def __init__(self, num_labels_x, num_labels_y, X, Y, extra_cluster=False):
        if extra_cluster:
            self.X, self.Y = X, Y
            x_series = np.mean(self.X, axis=1)
            x_series[-1] = num_labels_x
            self.X = pd.concat([self.X, pd.Series(x_series)])

            y_series = np.mean(self.Y, axis=1)
            y_series[-1] = num_labels_y
            self.Y = pd.concat([self.Y, pd.Series(y_series)])

            self.distances = np.zeros((num_labels_x+1, num_labels_y+1))
            self.num_labels_x, self.num_labels_y = num_labels_x+1, num_labels_y+1
        else:
            self.X, self.Y = X, Y
            self.distances = np.zeros((num_labels_x, num_labels_y))
            self.num_labels_x, self.num_labels_y = num_labels_x, num_labels_y

    @abstractmethod
    def calculate(self):
        ...
