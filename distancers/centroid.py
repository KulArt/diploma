import numpy as np
from scipy.spatial import distance

from distancers.abstract import AbstractDistancer


class CentroidDistancer(AbstractDistancer):
    def __init__(self, num_labels_x, num_labels_y, X, Y):
        super().__init__(num_labels_x, num_labels_y, X, Y)

    def calculate(self):
        for i in range(self.num_labels_x):
            for j in range(self.num_labels_y):
                cur_x = self.X[self.X['label'] == i].drop('label', axis=1).to_numpy().T
                cur_y = self.Y[self.Y['label'] == j].drop('label', axis=1).to_numpy().T
                if cur_y.shape[1] == 0 or cur_x.shape[1] == 0:
                    self.distances[i][j] = 1.5
                else:
                    centroid_x = np.mean(cur_x, axis=1) + 1e-7
                    centroid_y = np.mean(cur_y, axis=1) + 1e-7
                    self.distances[i][j] = distance.cosine(centroid_x, centroid_y)
        self.distances /= np.sum(self.distances, axis=0)
        # for j in range(self.num_labels_y):
        #     self.distances[:, j] = np.exp(self.distances[:, j]) / sum(np.exp(self.distances[:, j]))
        return self.distances
