from distancers.abstract import AbstractDistancer

import numpy as np


class SingularDistancer(AbstractDistancer):
    def __init__(self, num_labels_x, num_labels_y, X, Y):
        super().__init__(num_labels_x, num_labels_y, X, Y)

    def calculate(self):
        for i in range(self.num_labels_x):
            for j in range(self.num_labels_y):
                cur_x = self.X[self.X['label'] == i].drop('label', axis=1).to_numpy().T
                cur_y = self.Y[self.Y['label'] == j].drop('label', axis=1).to_numpy().T
                if cur_y.shape[1] == 0 or cur_x.shape[1] == 0:
                    self.distances[i][j] = self.distances[j][i] = 1
                else:
                    qx, _ = np.linalg.qr(cur_x)
                    qy, _ = np.linalg.qr(cur_y)
                    u, s, v = np.linalg.svd(qx.T @ qy)
                    self.distances[i][j] = self.distances[j][i] = 1./np.sqrt(1 - s[-1]**2)
        for j in range(self.num_labels_y):
            self.distances[:, j] = np.exp(self.distances[:, j]) / sum(np.exp(self.distances[:, j]))
        return self.distances
