from clusterizer.abstract import AbstractClusterizer

from sklearn.cluster import AgglomerativeClustering
from sklearn.ensemble import RandomForestClassifier


class RFClusterizer(AbstractClusterizer):
    def __init__(self, max_num_clusters=15, in_size=None):
        super().__init__(max_num_clusters)
        self.clusterizer = AgglomerativeClustering(n_clusters=max_num_clusters,
                                                   linkage='ward', compute_distances=True)
        self.model = RandomForestClassifier(n_estimators=max_num_clusters*5, max_depth=3)

    def fit_predict(self, df, emb_df, prev_emb=None, prev_labels=None, with_val=False):
        if emb_df.shape[0] < 2:
            return 1, [1]
        else:
            self.clusterizer = AgglomerativeClustering(n_clusters=min(self.max_num_clusters, emb_df.shape[0]),
                                                       linkage='ward', compute_distances=True)
        if prev_emb is not None:
            output = self.__generate__probs__(df, emb_df, prev_emb=prev_emb.loc[:, ~prev_emb.columns.isin(['label'])],
                                              prev_labels=prev_emb['label'], with_val=with_val)

            return min(self.max_num_clusters, emb_df.shape[0]), self.clusterizer.fit_predict(output)
        else:
            return min(self.max_num_clusters, emb_df.shape[0]), self.clusterizer.fit_predict(emb_df)

    def __generate__probs__(self, df, emb_df, prev_emb=None, prev_labels=None, with_val=False, num_epochs=3):
        X_train, y_train = prev_emb.copy(), prev_labels
        X_train.fillna(0., inplace=True)
        try:
            self.model.fit(X_train, y_train)
        except:
            print(X_train.isna())
        return self.model.predict_proba(emb_df)
