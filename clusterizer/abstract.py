from abc import abstractmethod


class AbstractClusterizer:
    def __init__(self, max_num_clusters):
        self.max_num_clusters = max_num_clusters
        ...

    @abstractmethod
    def fit_predict(self, df, emb_df):
        ...
