from clusterizer.abstract import AbstractClusterizer

import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords

import gensim
from gensim.models import LdaModel, CoherenceModel

nltk.download('wordnet')
nltk.download('stopwords')
nltk.download('omw-1.4')


class LDAClusterizer(AbstractClusterizer):
    def __init__(self, max_num_clusters, in_size=None):
        super().__init__(max_num_clusters)
        self.stopwords = set(stopwords.words('russian'))
        self.wordnet_lemmatizer = WordNetLemmatizer()

    def fit_predict(self, df, emb_df, prev_emb=None, prev_labels=None):
        cleaned_group = self.data_clean(df['title'])
        if cleaned_group.shape[0] == 0:
            return self.max_num_clusters, []
        dictionary = gensim.corpora.Dictionary(cleaned_group)
        dictionary.filter_extremes(no_below=1, no_above=0.8, keep_n=None)
        bow_corpus = [dictionary.doc2bow(doc) for doc in cleaned_group]
        try:
            lda_model = LdaModel(bow_corpus, id2word=dictionary, num_topics=self.max_num_clusters, offset=2,
                                 random_state=100, update_every=1, passes=2, alpha='auto', eta="auto",
                                 per_word_topics=True)
        except:
            print(bow_corpus, cleaned_group)
            return self.max_num_clusters, []
        labels = []

        for bow in bow_corpus:
            best_theme, best_prob = -1, 0.
            for theme, prob in lda_model.get_document_topics(bow):
                if prob > best_prob:
                    best_prob = prob
                    best_theme = theme
            labels.append(best_theme)

        return self.max_num_clusters, labels

    def __tokenize_lemma_stopwords__(self, text):
        tokens = nltk.tokenize.word_tokenize(text.lower())
        tokens = [t for t in tokens if t.isalpha()]
        tokens = [self.wordnet_lemmatizer.lemmatize(t) for t in tokens]
        tokens = [t for t in tokens if len(t) > 2]
        tokens = [t for t in tokens if t not in self.stopwords]

        return tokens

    def data_clean(self, data):
        data = data.apply(self.__tokenize_lemma_stopwords__)
        return data
