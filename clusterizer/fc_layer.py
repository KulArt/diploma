from clusterizer.abstract import AbstractClusterizer

from sklearn.cluster import AgglomerativeClustering

import torch
import torch.nn as nn
import torch.nn.functional as F

from transformers import BertModel, BertTokenizer
from transformers import AdamW

from torch.utils.data import TensorDataset, DataLoader

from transformers import logging

logging.set_verbosity(40)


class Net(nn.Module):
    def __init__(self, out_size, hidden_size=50, in_size=None):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(in_size, hidden_size)
        self.fc2 = nn.Linear(hidden_size, out_size)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.softmax(self.fc2(x))
        return x


class BertBasedClusterizer(AbstractClusterizer):
    def __init__(self, max_num_clusters=15, in_size=None):
        super().__init__(max_num_clusters)
        self.clusterizer = AgglomerativeClustering(n_clusters=max_num_clusters,
                                                   linkage='ward', compute_distances=True)
        # rint(in_size)
        self.model = Net(out_size=max_num_clusters, in_size=in_size)
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.model = self.model.to(self.device)
        self.opt = AdamW(self.model.parameters(), lr=3e-4)

    def fit_predict(self, df, emb_df, prev_emb=None, prev_labels=None, with_val=False):
        if emb_df.shape[0] < 2:
            return 1, [1]
        else:
            self.clusterizer = AgglomerativeClustering(n_clusters=min(self.max_num_clusters, emb_df.shape[0]),
                                                       linkage='ward', compute_distances=True)
        if prev_emb is not None:
            self.__generate__probs__(df, emb_df, prev_emb=prev_emb.loc[:, ~prev_emb.columns.isin(['label'])],
                                     prev_labels=prev_emb['label'], with_val=with_val)
            test_ds = TensorDataset(torch.from_numpy(emb_df.copy().to_numpy()))
            test_dl = DataLoader(test_ds, batch_size=8, shuffle=False)
            outputs = []
            for x_batch in test_dl:
                output = self.model(x_batch[0].float())
                outputs.append(output)
            output = torch.cat(tuple(outputs), 0).detach().numpy()

            return min(self.max_num_clusters, emb_df.shape[0]), self.clusterizer.fit_predict(output)
        else:
            return min(self.max_num_clusters, emb_df.shape[0]), self.clusterizer.fit_predict(emb_df)

    def __generate__probs__(self, df, emb_df, prev_emb=None, prev_labels=None, with_val=False, num_epochs=10):
        if with_val:
            train_len = int(0.7 * emb_df.shape[0])
            X_train, y_train = prev_emb[:train_len], prev_labels[:train_len]
            train_ds = TensorDataset(*map(lambda x: torch.from_numpy(x.copy()), [X_train.to_numpy(), y_train.to_numpy()]))
            train_dl = DataLoader(train_ds, batch_size=8, shuffle=True, drop_last=True)

            X_val, y_val = prev_emb[train_len:], prev_labels[train_len:]
            val_ds = TensorDataset(*map(lambda x: torch.from_numpy(x.copy()), [X_val.to_numpy(), y_val.to_numpy()]))
            val_dl = DataLoader(val_ds, batch_size=8, shuffle=False)
        else:
            X_train, y_train = prev_emb, prev_labels
            train_ds = TensorDataset(*map(lambda x: torch.from_numpy(x.copy()), [X_train.to_numpy(), y_train.to_numpy()]))
            train_dl = DataLoader(train_ds, batch_size=8, shuffle=True, drop_last=True)

        for i in range(num_epochs):
            loss_log, acc_log = self.__train__(train_dl)
            # print("Train loss:", loss_log)
            # print("Train Acc:", acc_log)
            if with_val:
                loss_log, acc_log = self.__test__(val_dl)
                # print("Val loss:", loss_log)
                # print("Val Acc:", acc_log)

    def __train__(self, train_dl):
        loss_log, acc_log = [], []
        self.model.train()
        for x_batch, y_batch in train_dl:
            self.opt.zero_grad()
            output = self.model(x_batch.float())
            loss = F.cross_entropy(output, y_batch)
            loss.backward()
            self.opt.step()
            pred = torch.max(output, 1).indices
            acc = (pred == y_batch).sum() / y_batch.shape[0]
            acc_log.append(acc.item())
            loss = loss.item()
            loss_log.append(loss)
        return loss_log, acc_log

    @torch.inference_mode()
    def __test__(self, val_dl):
        loss_log, acc_log = [], []
        self.model.eval()
        for x_batch, y_batch in val_dl:
            output = self.model(x_batch.float())
            loss = F.cross_entropy(output, y_batch)
            pred = torch.max(output, 1).indices
            acc = (pred == y_batch).sum() / y_batch.shape[0]
            acc_log.append(acc.item())
            loss = loss.item()
            loss_log.append(loss)
        return loss_log, acc_log
