from clusterizer.abstract import AbstractClusterizer

from sklearn.cluster import AgglomerativeClustering


class AgglomerativeClusterizer(AbstractClusterizer):
    def __init__(self, max_num_clusters=15, in_size=None):
        super().__init__(max_num_clusters)
        self.clusterizer = AgglomerativeClustering(n_clusters=max_num_clusters,
                                                   linkage='ward', compute_distances=True)

    def fit_predict(self, df, emb_df, get_optimal=False, prev_emb=None, prev_labels=None):
        if emb_df.shape[0] < 2:
            return 1, [1]
        self.clusterizer = AgglomerativeClustering(n_clusters=min(self.max_num_clusters, emb_df.shape[0]),
                                                   linkage='ward', compute_distances=True)
        self.clusterizer.fit_predict(emb_df)
        if get_optimal:
            n_clusters = self.__get_optimal_clusters_num__(self.clusterizer.distances_,
                                                           max_num_clusters=self.max_num_clusters)
            self.clusterizer = AgglomerativeClustering(n_clusters=min(n_clusters, emb_df.shape[0]), linkage='ward')
            return n_clusters, self.clusterizer.fit_predict(emb_df)

        self.clusterizer = AgglomerativeClustering(n_clusters=min(self.max_num_clusters, emb_df.shape[0]),
                                                   linkage='ward')
        return min(self.max_num_clusters, emb_df.shape[0]), self.clusterizer.fit_predict(emb_df)

    @staticmethod
    def __get_optimal_clusters_num__(dist, N=2, max_num_clusters=15):
        print(max_num_clusters)
        n_clusters = 0
        prev_delta = dist[1] - dist[0]
        for i in range(1, len(dist) - 1):
            cur_delta = dist[i + 1] - dist[i]
            if cur_delta > N * prev_delta:
                n_clusters = i
                break
            prev_delta = cur_delta
        return min(len(dist) - n_clusters, max_num_clusters)
